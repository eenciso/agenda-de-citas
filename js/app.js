// Variables Globales
let DB;

// Selectores de la interfaz
const form = document.querySelector('form'),
	  nombreMascota = document.querySelector('#mascota'),
	  nombreCliente = document.querySelector('#cliente'),
	  telefono = document.querySelector('#telefono'),
	  fecha = document.querySelector('#fecha'),
	  hora = document.querySelector('#hora'),
	  sintomas = document.querySelector('#sintomas'),
	  citas = document.querySelector('#citas'),
	  headingAdministra = document.querySelector('#administra');

// Esperar por el DOM Ready
document.addEventListener('DOMContentLoaded', () => {
	// Crear la Base de Datos .open( nombre , version_de_la_bd )
	let nuevaDB = window.indexedDB.open('citas', 1);
	// Manejo de Errores
	nuevaDB.onerror = function () {
		console.log('¡Hubo un error!');
	}
	// Asignar a la base de Datos
	nuevaDB.onsuccess = function () {
		DB = nuevaDB.result;
		mostrarCitas();
	}
	// Crea el Schema de la Base de Datos
	nuevaDB.onupgradeneeded = function (e) {
		// Instancia a la Base de Datos
		let db = e.target.result;
		// Definir ObjectStore .createObjectStore( nombreBD, opciones )
		// keyPath -> Indice de la Base de Datos
		let objectStore = db.createObjectStore('citas', { keyPath: 'key', autoIncrement: true,  });
		// Crear los indices y campos de la Base de Datos
		// createIndex : 3 parametros, nombre, keypath y opciones
		objectStore.createIndex('mascota', 'mascota', { unique: false });
		objectStore.createIndex('cliente', 'cliente', { unique: false });
		objectStore.createIndex('telefono', 'telefono', { unique: false });
		objectStore.createIndex('fecha', 'fecha', { unique: false });
		objectStore.createIndex('hora', 'hora', { unique: false });
		objectStore.createIndex('sintomas', 'sintomas', { unique: false });
	}

	// Enviando el Formulario
	form.addEventListener('submit', agregarDatos);
	// Recuperando datos del Formulario
	function agregarDatos (e) {
		e.preventDefault();
		const nuevaCita = {
			mascota : nombreMascota.value,
			cliente : nombreCliente.value,
			telefono : telefono.value,
			fecha : fecha.value,
			hora : hora.value,
			sintomas : sintomas.value
		}

		// Transaccion
		let transaccion = DB.transaction(['citas'], 'readwrite');
		// ObjectStore para Insertar Datos
		let objectStore = transaccion.objectStore('citas');
		// Eviar Peticion a la Base de Datos para Insertar Datos
		let peticion = objectStore.add(nuevaCita);

		console.log(peticion);

		peticion.onsuccess = () => {
			form.reset();
		}

		// Transaccion completada
		transaccion.oncomplete = () => {
			console.log('¡Cita Agendada!');
			mostrarCitas();
		}
		// Transaccion con error
		transaccion.onerror = () => {
			console.log('¡Hubo un Error!');
		}
	}

	function mostrarCitas () {
		// Limpiar Citas Agendadas del HTML
		while (citas.firstChild) {
			citas.removeChild(citas.firstChild);
		}
		// Se crea el Object Store
		let objectStore = DB.transaction('citas').objectStore('citas');
		// Abrir peticion con ObjectCursor
		objectStore.openCursor().onsuccess = function (e) {
			// Accediendo a los datos
			let cursor = e.target.result;
			// Comprobar que existan Registros
			if (cursor) {
				let citaHTML = document.createElement('li');
				// Se asigna un atributo presonalizado con el Id del Registro
				citaHTML.setAttribute('data-cita-id', cursor.value.key);
				citaHTML.classList.add('list-group-item');
				citaHTML.innerHTML = `
					<p class="font-weight-bold">Mascota: <span class="font-weight-normal">${cursor.value.mascota}</span></p>
					<p class="font-weight-bold">Cliente: <span class="font-weight-normal">${cursor.value.cliente}</span></p>
					<p class="font-weight-bold">Telefono: <span class="font-weight-normal">${cursor.value.telefono}</span></p>
					<p class="font-weight-bold">Fecha: <span class="font-weight-normal">${cursor.value.fecha}</span></p>
					<p class="font-weight-bold">Hora: <span class="font-weight-normal">${cursor.value.hora}</span></p>
					<p class="font-weight-bold">Sintomas: <span class="font-weight-normal">${cursor.value.sintomas}</span></p>
				`;
				// Boton para borrar registros
				const botonBorrar = document.createElement('button');
				botonBorrar.classList.add('borrar', 'btn', 'btn-danger');
				botonBorrar.innerHTML = '<span aria-hidden="true">x</span> Borrar';
				// Se asigna la funcion Borrar al Boton cuando este sea activado
				botonBorrar.onclick = borrarCita;
				citaHTML.appendChild(botonBorrar);
				// Append en el Padre para insertar el elemento li con la información
				citas.appendChild(citaHTML);
				// Se le indica al cursor que continue en caso de que haya mas registros
				cursor.continue();
			} else {
				// Se muestra heading indicando que no hay registros
				if (!citas.firstChild) {
					headingAdministra.textContent = 'Agrega citas para comenzar';
					let listado = document.createElement('p');
					listado.classList.add('text-center');
					listado.textContent = 'No se ecnontraron registros.'
					citas.appendChild(listado);
				} else {
					// Se muetra el heading cuando existen registros
					headingAdministra.textContent = 'Administra tus Citas';
				}
			}
		}
	}

	function borrarCita (e) {
		// Obtiene el Id de la cita agendada
		let citaId = Number(e.target.parentElement.getAttribute('data-cita-id'));
		// Transaccion
		let transaccion = DB.transaction(['citas'], 'readwrite');
		// ObjectStore para Eliminar Datos
		let objectStore = transaccion.objectStore('citas');
		// Eviar Peticion a la Base de Datos para Eliminar Datos
		let peticion = objectStore.delete(citaId);
		// Quitar cita del DOM
		transaccion.oncomplete = () => {
			e.target.parentElement.parentElement.removeChild( e.target.parentElement );
			console.log(`Se eliminó la cita con el ID: ${citaId}`);

			// Se muestra heading indicando que no hay registros
			if (!citas.firstChild) {
				headingAdministra.textContent = 'Agrega citas para comenzar';
				let listado = document.createElement('p');
				listado.classList.add('text-center');
				listado.textContent = 'No se ecnontraron registros.'
				citas.appendChild(listado);
			} else {
				// Se muetra el heading cuando existen registros
				headingAdministra.textContent = 'Administra tus Citas';
			}
		}
	}
});



